package fr.mathias.levostre.player;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import fr.mathias.levostre.util.ScanObject;
import fr.mathias.levostre.util.Validator;

import static org.mockito.Mockito.*;

import java.util.ArrayList;

import static org.junit.Assert.*;

public class SimplePlayerTest {
	
	ScanObject scanObjectMock;
	Validator validatorMock; 
	SimplePlayer simplePlayer = new SimplePlayer("test", validatorMock);
	ArrayList<Player> listPlayer = new ArrayList<Player>();
	String nameOpponent = new String("opponent"); 
	SimplePlayer opponent = new SimplePlayer(nameOpponent, validatorMock);
	
	
	@Before
	public void setup(){
		scanObjectMock = Mockito.mock(ScanObject.class);
		validatorMock  = Mockito.mock(Validator.class);
		when(scanObjectMock.scan("Composer un nom commun en fonction du pot commun, caractère '*' pour revenir au menu principale:")).thenReturn("*").thenReturn("balbla");
		when(scanObjectMock.scan("Choisir un de vos adverssaires en fonction de son numero :")).thenReturn("*").thenReturn("juju").thenReturn("155").thenReturn("2"); 
		when(scanObjectMock.scan("Choisir un mot de '"+ nameOpponent + "' en fonction de son numéro :")).thenReturn("juju").thenReturn("155").thenReturn("1");
		when(scanObjectMock.scan("Proposer un mot en fonction du mot de votre adversaire et du pot commun , '*' pour revenir au menu principal: ")).thenReturn("balbla");
		when(scanObjectMock.scan("1) Utiliser le pot commu   2) Utiliser un des mots de vos adversaire  3) Ettendre un de vos mots    4) Passer")).thenReturn("juju").thenReturn("8").thenReturn("1"); 
		when(scanObjectMock.scan("Ettendez votre mots, '*' pour revenir au menu principal: ")).thenReturn("balbla");
		when(scanObjectMock.scan("Choisir un de vos mots en fonction de son numéro :")).thenReturn("1");
		when(validatorMock.validWord("balbla", "blabla")).thenReturn(true);
		when(validatorMock.stealWordValidate("balbla", "blabla", "blabla")).thenReturn(true);
		when(validatorMock.extendWordValidate(anyString(), anyString(), anyString(), anyString() )).thenReturn(true);
		simplePlayer.setScan(scanObjectMock);
		simplePlayer.setValidator(validatorMock);
		opponent.getListWord().add("blabla");
		simplePlayer.getListWord().add("blabla");
		simplePlayer.getListWord().add("f");
		listPlayer.add(simplePlayer);
		listPlayer.add(opponent );
		simplePlayer.setListPlayer(listPlayer);
		simplePlayer.setCommunPot("blabla");
	}
	
	@Test
	public void testProposedWord(){
		assertFalse(simplePlayer.proposeWord());
		assertTrue(simplePlayer.proposeWord());
	}
	
	@Test
	public void testStealOpponentWord(){
		assertFalse(simplePlayer.stealOpponentWord()); 
		assertTrue(simplePlayer.stealOpponentWord());
	}
	
	@Test
	public void testExtendWord(){
		when(scanObjectMock.scan("Voulez vous utiliser un des mots de votre adversaire : [ o / n ] ")).thenReturn("n");
		assertTrue(simplePlayer.extendWord());
		
		when(scanObjectMock.scan("Voulez vous utiliser un des mots de votre adversaire : [ o / n ] ")).thenReturn("o");
		assertFalse(simplePlayer.extendWord());
		assertTrue(simplePlayer.extendWord());
		when(scanObjectMock.scan("Voulez vous utiliser un des mots de votre adversaire : [ o / n ] ")).thenReturn("n");
		simplePlayer.setCommunPot("bcjdqsbou");
		when(scanObjectMock.scan("Ettendez votre mots, '*' pour revenir au menu principal: ")).thenReturn("fou");
		when(scanObjectMock.scan("Choisir un de vos mots en fonction de son numéro :")).thenReturn("2");
		assertTrue(simplePlayer.extendWord());
	}
	
	@Test
	public void testPlaye(){
		simplePlayer.playe();
	}
	
}
