package fr.mathias.levostre.dictionary;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*; 

public class DictionaryTest {
	private final static String DICO_FILE = "/dico.txt"; 
	private IDictionary dictionary; 
	
	@Before
	public void setup(){
		dictionary = new ProxyDictionary(DICO_FILE);
	}
	
	@Test
	public void testisWord(){
		assertTrue(dictionary.isWord("decoration"));
		assertFalse(dictionary.isWord("bubub"));
		assertTrue(dictionary.isWord("abaque"));
	}
	
	@Test
	public void testBruteForceSearch(){
		assertEquals("abaque", dictionary.bruteForceSearch("bttttaaque"));
	}

}
