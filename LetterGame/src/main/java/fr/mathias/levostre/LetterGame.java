package fr.mathias.levostre;

import java.nio.charset.StandardCharsets;
import java.util.Scanner;

import fr.mathias.levostre.player.CompositePlayer;
import fr.mathias.levostre.player.PlayerInit;

public class LetterGame {


	public LetterGame() {}
	
	@SuppressWarnings("resource")
	public void run(){
		Scanner scan = new Scanner(System.in, StandardCharsets.UTF_8.name()); 
		PlayerInit playerInit = new PlayerInit();
		playerInit.run();
		System.out.println("Appuyer sur entrer pour commencer le jeu ...");
		scan.nextLine(); 
		CompositePlayer players = playerInit.compositePlayer;
		players.setCommunPot(playerInit.startedCommunpot);
		players.playe();
	}
}
