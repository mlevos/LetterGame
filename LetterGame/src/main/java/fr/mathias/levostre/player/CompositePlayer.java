package fr.mathias.levostre.player;

import java.util.ArrayList;

import org.apache.commons.lang3.RandomStringUtils;

import fr.mathias.levostre.util.PrintObject;

public class CompositePlayer implements IPlayer{
	
	private ArrayList<Player> listPlayer = new ArrayList<Player>();
	private CharSequence communPot; 
	private Player winner; 
	
	@Override
	public void playe(){
		boolean end = false;
		while(!end){
			for(Player player: listPlayer){
				String randomLetter = RandomStringUtils.random(2, 97, 122, true, false);
				communPot = communPot+randomLetter;
				player.setCommunPot(communPot);
				player.setListPlayer(listPlayer);
				player.playe();
				
				if(player.getListWord().size() >= 10){
					end = true;
					winner = player; 
					break; 
				}
				communPot = player.getCommunPot();
			}
		}
		PrintObject.clearConsole();
		PrintObject.printTitle();
		System.out.println("Le gagnant est => "+ winner.getName());	
	}
	
	public void addPlayer(Player player){
		listPlayer.add(player);
	}
	public void setCommunPot(CharSequence communPot) {
		this.communPot = communPot;
	}

}
