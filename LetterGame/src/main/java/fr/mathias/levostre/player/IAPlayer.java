package fr.mathias.levostre.player;
import fr.mathias.levostre.player.command.CommandFactory;
import fr.mathias.levostre.util.PrintObject;
import fr.mathias.levostre.util.Validator;

public class IAPlayer extends Player{

	public IAPlayer(String name, Validator validator) {
		super(name, validator);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void playe() {
		PrintObject.printHedaer(listPlayer, communPot);
		System.out.println("Tour du joueur => " + name);
		System.out.print("Attente fin de tour IA ..."); 
		try {
		    Thread.sleep(2000);                 //1000 milliseconds is one second.
		} catch(InterruptedException ex) {
		    Thread.currentThread().interrupt();
		}
		CommandFactory invoker = new CommandFactory(this);
		invoker.getCommandPlayer(listCommands[0]).execute(); 	
	}

	@Override
	public boolean proposeWord() {
		String word = validator.getDictionary().bruteForceSearch(communPot);
		if(word == null)
			return false;
		winProcess(word);
		return true; 
	}

	@Override
	public boolean stealOpponentWord() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean extendWord() {
		// TODO Auto-generated method stub
		return false;
	}

}
