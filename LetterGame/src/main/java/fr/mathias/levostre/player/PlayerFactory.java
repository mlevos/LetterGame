package fr.mathias.levostre.player;

import fr.mathias.levostre.dictionary.ProxyDictionary;
import fr.mathias.levostre.util.Validator;

public class PlayerFactory {
	
	private Validator validator = new Validator(new ProxyDictionary("/dico.txt"));
	
	public Player getPlayer(String type, String name){
		if(type.equals("simple")){
			return new SimplePlayer(name, validator); 
		}else if(type.equals("ia")){
			return new IAPlayer(name, validator); 
		}
		else{
			return null;
		}
	}

}
