package fr.mathias.levostre.player.command;

import fr.mathias.levostre.player.Player;

public abstract class CommandPlayer implements ICommandPlayer{
	Player player; 
	
	public CommandPlayer(Player player) {
		this.player = player; 
	}
}
