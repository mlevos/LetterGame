package fr.mathias.levostre.player.command;

import fr.mathias.levostre.player.Player;

public class StealWordCommand extends CommandPlayer{

	public StealWordCommand(Player player) {
		super(player);
	}

	@Override
	public boolean execute() {
		return player.stealOpponentWord();
	}


}
