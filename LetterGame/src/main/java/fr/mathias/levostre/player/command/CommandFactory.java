package fr.mathias.levostre.player.command;

import java.util.HashMap;

import fr.mathias.levostre.player.Player;


public class CommandFactory {
	
	Player player; 
	HashMap<String, ICommandPlayer> mapCommandPlayer = new HashMap<String, ICommandPlayer>();
	
	
	public CommandFactory(Player player) {
		this.player = player;
		setCommandPlayer();
	}
	public void addCommand(String key, ICommandPlayer iCommandPlayer){
		mapCommandPlayer.put(key, iCommandPlayer);
	}
	
	public void setCommandPlayer(){
		addCommand("proposed", new ProposedWordCommand(player));
		addCommand("steal_word", new StealWordCommand(player));
		addCommand("extend_word", new ExtendWordCommand(player));
	}
	
	public ICommandPlayer getCommandPlayer(String key){
		return mapCommandPlayer.get(key); 
	}
	
	

}
