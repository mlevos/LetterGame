package fr.mathias.levostre.player.command;

import fr.mathias.levostre.player.Player;

public class ProposedWordCommand extends CommandPlayer{

	public ProposedWordCommand(Player player) {
		super(player);
	}

	@Override
	public boolean execute() {
		return player.proposeWord();
	}

}
