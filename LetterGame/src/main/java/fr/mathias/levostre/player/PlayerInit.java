package fr.mathias.levostre.player;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;

import fr.mathias.levostre.util.PrintObject;
import fr.mathias.levostre.util.ScanObject;
import fr.mathias.levostre.util.Validator;

public class PlayerInit{
	
	private final static int  MAX_SIMPLE_PLAYER = 4;
	private final static int  MAX_IA_PLAYER = 4;
	private final static String ERROR_MESSAGE_NB_SIMPLE_PLAYER = "Mauvaise entrée , vous ne pouvez être plus de " + Integer.toString(MAX_IA_PLAYER) + " joueurs virtuel";
	private final static String ERROR_MESSAGE_NB_SIMPLE_IA = "Mauvaise entrée , vous ne pouvez être plus de " + Integer.toString(MAX_SIMPLE_PLAYER) + " joueurs";
	private ScanObject scanObject = new ScanObject(new Scanner(System.in, StandardCharsets.US_ASCII.name()));
	private int nbSimplePlayer;
	private int nbIAPlayer;
	private Map<Character, String> mapNamePlayer = new HashMap<Character, String>();
	public CharSequence startedCommunpot;
	public CompositePlayer compositePlayer = new CompositePlayer();
	public PlayerFactory playerFactory = new PlayerFactory(); 
	
	public PlayerInit() {
		
	}
	
	public void run(){
		setSimplePlayerNumber(false);
		setListSimplePlayerName();
		setIAPlayerNumber(false);
		setListIAPlayerName(); 
		sortHasmap();
	}
	
	public void setIAPlayerNumber(boolean printError){
		PrintObject.clearConsole();
		PrintObject.printTitle();
		if(printError)
			System.out.println(ERROR_MESSAGE_NB_SIMPLE_IA);
		String stringNumber = scanObject.scan("Entrer le nombre de joueur virtuel");
		if(!Validator.isNumber(stringNumber)){
			setSimplePlayerNumber(true);
		}
		else{
			if(Integer.parseInt(stringNumber) < 0 || Integer.parseInt(stringNumber) >  MAX_IA_PLAYER){
				setIAPlayerNumber(true);
			}else{
				nbIAPlayer = Integer.parseInt(stringNumber);
			}
		}
	}
	
	public void setSimplePlayerNumber(boolean printError){
		PrintObject.clearConsole();
		PrintObject.printTitle();
		if(printError)
			System.out.println(ERROR_MESSAGE_NB_SIMPLE_PLAYER);
		String stringNumber = scanObject.scan("Entrer le nombre de joueur");
		if(!Validator.isNumber(stringNumber)){
			setSimplePlayerNumber(true);
		}
		else{
			if(Integer.parseInt(stringNumber) < 1 || Integer.parseInt(stringNumber) >  MAX_SIMPLE_PLAYER){
				setSimplePlayerNumber(true);
			}else{
				nbSimplePlayer = Integer.parseInt(stringNumber);
			}
		}
	}
	public void setListSimplePlayerName(){
		PrintObject.clearConsole();
		PrintObject.printTitle();
		for(int i=0; i<nbSimplePlayer ; i++){
			String namePlayer = scanObject.scan("Entrer le nom du joueur");
			while(!StringUtils.isAsciiPrintable(namePlayer)){
				System.out.println("Votre nom ne doit comporter que des caractère ASCII");
				namePlayer = scanObject.scan("Entrer le nom du joueur");
			}
			Character randomletter = RandomStringUtils.random(1, 97, 122, true, false).charAt(0);
			if(namePlayer.isEmpty())
				namePlayer = "PlayerAnonimus";
			mapNamePlayer.put(randomletter, namePlayer);
		}
	}
	public void setListIAPlayerName(){
		for(int i=0; i<nbIAPlayer ; i++){
			String namePlayer = "éia_" + Integer.toString(i+1); 
			Character randomletter = RandomStringUtils.random(1, 97, 122, true, false).charAt(0);
			mapNamePlayer.put(randomletter, namePlayer);
		}
	}
	
	public void sortHasmap(){
		PrintObject.clearConsole();
		PrintObject.printTitle();
		Map<Character, String> treeMap = new TreeMap<Character, String>(mapNamePlayer);
		System.out.println("Ordre de passage :");
		for (Map.Entry<Character, String> entry : treeMap.entrySet()) {
			if(StringUtils.isAsciiPrintable(entry.getValue())){
				System.out.println(entry.getKey() + " : " + entry.getValue());
				compositePlayer.addPlayer(playerFactory.getPlayer("simple", entry.getValue()));
			}else{
				String name_ia = entry.getValue().replaceAll("é", ""); 
				System.out.println(entry.getKey() + " : " + name_ia);
				compositePlayer.addPlayer(playerFactory.getPlayer("ia", name_ia));
			}
			
		}
		startedCommunpot = getStringRepresentation(new ArrayList<>(mapNamePlayer.keySet()));
	}
	
	public CharSequence getStringRepresentation(List<Character> list)
	{   
	    StringBuilder builder = new StringBuilder(list.size());
	    for(Character ch: list)
	    {
	        builder.append(ch);
	    }
	    return builder.toString();
	}
	public void setScanObject(ScanObject scanObject) {
		this.scanObject = scanObject;
	}
	public void setNbSimplePlayer(int nbSimplePlayer) {
		this.nbSimplePlayer = nbSimplePlayer;
	}
	public int getNbSimplePlayer() {
		return nbSimplePlayer;
	}
	public Map<Character, String> getMapNamePlayer() {
		return mapNamePlayer;
	}
	public int getNbIAPlayer() {
		return nbIAPlayer;
	}
	public void setNbIAPlayer(int nbIAPlayer) {
		this.nbIAPlayer = nbIAPlayer;
	}

}
