package fr.mathias.levostre.player;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;

import fr.mathias.levostre.util.Validator;


public abstract class Player implements IPlayer{
	
	protected String name;
	protected List<String> listWord = new ArrayList<String>();
	protected ArrayList<Player> listPlayer = new ArrayList<Player>(); 
	protected CharSequence communPot ; 
	protected String[] listCommands = new String[]{"proposed", "steal_word", "extend_word"}; 
	//protected Validator validator = new Validator(new ProxyDictionary("/dico.txt"));
	protected Validator validator;
 	
	public Player(String name, Validator validator) {
		this.name = name;
		this.validator = validator; 
	}
	public abstract boolean proposeWord();
	public abstract boolean stealOpponentWord();
	public abstract boolean extendWord();
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<String> getListWord() {
		return listWord;
	}
	public void setCommunPot(CharSequence communPot) {
		this.communPot = communPot;
	}
	public void setListPlayer(ArrayList<Player> listPlayer) {
		this.listPlayer = listPlayer;
	}
	protected int getNbPlayer(){
		return listPlayer.size(); 
	}
	public CharSequence getCommunPot() {
		return communPot;
	}
	protected void winProcess(String word){
		listWord.add(word);
		for (int i = 0; i < word.length(); i++) {
			communPot = communPot.toString().replaceFirst(""+word.charAt(i)+"", "");
	    }
		String randomLetter = RandomStringUtils.random(1, 97, 122, true, false);
		communPot = communPot+randomLetter;
	}
	public void setValidator(Validator validator) {
		this.validator = validator;
	}
	
	

}
