## Package Dictionary 
#### Responsabilité dans le code 
Les classes présentes dans ce package ont pour but de faire le lien entre l'interface externe, le fichier [`dico.txt`](LetterGame/src/main/resources/dico.txt), et le reste de l'application.

#### Patron de conception : Proxy 
Pour l'architecture de ce package j'ai décidé d'utiliser un des patrons de conception  [proxy](https://fr.wikibooks.org/wiki/Patrons_de_conception/Proxy), le `Virtual Proxy` . 
Ce choix a été fait pour éviter de charger le fichier dictionnaire en mémoire inutilement avant le premier appel d'une méthode. Ce qui permet de définir en amont l'objet de recherche dans le fichier txt sans le mettre en mémoire. 

Avantage : on garde une clarté de code tout en gardant une optimisation du programme. En effet, dans la classe [FactoryPlayer](LetterGame/src/main/java/fr/esiea/audrine/lapin/player/PlayerFactory.java) , utilisé uniquement dans [PlayerInit](LetterGame/src/main/java/fr/esiea/audrine/lapin/player/PlayerInit.java) , nous créons qu'un seul objet ProxyDictionary qui ne chargera en mémoire le fichier dictionnaire au premier appel de la méthode isWord.

#### Les classes 
*  [`IDictionary`](LetterGame/src/main/java/fr/esiea/audrine/lapin/dictionary/IDictionary.java):  Interface des objets dictionnaire 
* [`RealDictionary`](LetterGame/src/main/java/fr/esiea/audrine/lapin/dictionary/RealDictionary.java): Classe qui charge réellement le fichier txt en mémoire
* [`ProxyDictionary`](LetterGame/src/main/java/fr/esiea/audrine/lapin/dictionary/ProxyDictionary.java):  Classe virtuelle qui crée l'objet RealDictionary lors du premier appel d'une de ses méthodes et exécute la méthode correspondante de ce nouvel objet.


