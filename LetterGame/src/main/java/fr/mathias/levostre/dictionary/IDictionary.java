package fr.mathias.levostre.dictionary;

public interface IDictionary {
	public boolean isWord(String word);
	public String bruteForceSearch(CharSequence communPot);
}
