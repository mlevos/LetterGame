package fr.mathias.levostre.dictionary;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.text.Normalizer;
import java.text.Normalizer.Form;


import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

public class RealDictionary implements IDictionary{
	
	private FileInputStream input;
	

	public RealDictionary(String dicoFile) {
		
		InputStream in = this.getClass().getResourceAsStream(dicoFile);
		try {
			File tempFile = File.createTempFile("tmp", "tmp");
			FileOutputStream out = new FileOutputStream(tempFile);
			IOUtils.copy(in, out); 
			input = new FileInputStream(tempFile);
			
		} catch (IOException e) {
			e.printStackTrace();
		}finally {
			IOUtils.closeQuietly(in);
		}
		
		
	
	}	
	public static String sansAccents(String s) {
		return Normalizer.normalize(s, Form.NFD) 
				 .replaceAll("\\p{InCombiningDiacriticalMarks}+", "");
	}
	
	private boolean matchCommunPot(String word, CharSequence communPot){
		
		
		for(int i = 0; i < word.length(); i++){
			if(! StringUtils.contains(communPot, word.charAt(i))){
				return false; 
			}
		}
		return true;
		
	}
	@Override
	public boolean isWord(String word){
		// TODO Auto-generated method stub
		String line;
		try {
			input.getChannel().position(0);
			BufferedReader br = new BufferedReader(new InputStreamReader(input,StandardCharsets.UTF_8));
			while((line = br.readLine()) != null){
				if(StringUtils.isAsciiPrintable(line)){
					if(StringUtils.equals(line, word)){
						return true;	
					}
				}
				else{
					String decode_line = sansAccents(line);
					if(StringUtils.equals(decode_line, word)){
						return true;
					}
				}		
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
	@Override
	public String bruteForceSearch(CharSequence communPot){
		String line;
		try {
			input.getChannel().position(0);
			BufferedReader br = new BufferedReader(new InputStreamReader(input,StandardCharsets.UTF_8));
			while((line = br.readLine()) != null){
				if(line.length() < 3){
					continue; 
				}
				if(StringUtils.isAsciiPrintable(line)){
					if(matchCommunPot(line, communPot)){
						return line;	
					}
				}
				else{
					String decode_line = sansAccents(line);
					if(matchCommunPot(decode_line, communPot)){
						return decode_line;
					}
				}		
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

}
